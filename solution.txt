Make a new file called solution.txt inside s01-a1 folder and answer the following questions based on the image:

1. List the books Authored by Marjorie Green.

 - The Busy Executive's Data base Guide
 - You Can Combat Computer Stress!


2. List the books Authored by Michael O'Leary.

 - Cooking with Computers


3. Write the author/s of "The Busy Executive’s Database Guide".

 - Marjorie Green
 - Abraham Bennet

4. Identify the publisher of "But Is It User Friendly?".

 - Charyl Carson

5. List the books published by Algodata Infosystems.

 - The Busy Executive's Database Guide
 - Cooking with Computers
 - Straight Talk About Computers
 - But Is It User Freindly?
 - Secrets of Silicon Valley
 - Net Etiquette
